# coding: utf-8
class CreateRaces < ActiveRecord::Migration
  def change
    create_table :races do |t|

      t.string  :nom, uniq: true, null: false

      # prefered class
      t.belongs_to :classe

      # 1 = I, 2 = TP, 4 = P, 8 = M, 16 = G, 32 = TG, 64 = C
      t.integer :taille

      t.integer :for
      t.integer :dex
      t.integer :con
      t.integer :int
      t.integer :sag
      t.integer :cha

      t.integer :vig
      t.integer :ref
      t.integer :vol

      # attaques: {nom => {degats => X, effets => StrBonus}}
      t.json    :attaques

      # competences: {nom => {value => X, condition => "*"}, ...}
      t.json    :competences
      # dons: {nom => {bonus => {competence}, type => "permanent/action X/...", description => Str, effet => Str }
      # note: don = don + pouvoir spécial + ... (préciser dans type)
      t.json    :dons
      # special : même que dons
      # note: bonus de compétences, de dons, de carac, resistance, capacité innée, ...
      t.json    :special

      t.string  :environements
      # 100 = bon, 0 = mauvais
      t.integer :alignement_morale
      # 100 = loyal, 0 = chaotique
      t.integer :alignement_ethique

      t.text    :description
      # langues = [Str, Str, ...]
      t.json    :langues

      t.timestamps null: false
    end
  end
end
